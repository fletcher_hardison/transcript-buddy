package com.example.fhard.kottest


import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [TextPicker.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [TextPicker.newInstance] factory method to
 * create an instance of this fragment.
 */
class TextPicker : Fragment() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view =  inflater!!.inflate(R.layout.fragment_text_picker, container, false)
        val audioBtn = view.findViewById(R.id.audioPickerBtn) as Button

        audioBtn.setOnClickListener {
           //Toast.makeText(context.applicationContext, "Was clicked", Toast.LENGTH_LONG).show()
            getAudioPath()
        }

        return view
    }


    fun getAudioPath()  {
        val intent = Intent()
                .setType("text/*")
                .setAction(Intent.ACTION_GET_CONTENT)

        startActivityForResult(Intent.createChooser(intent, "Select a file"), 123)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 123) {
            if ( resultCode == RESULT_OK) {
                val uri : Uri? = data?.data
                Toast.makeText(context.applicationContext, uri?.path, Toast.LENGTH_LONG).show()
                val cr = activity.contentResolver


                val toAdd = TextViewerFrag.newInstance(uri)
                val fm = fragmentManager.beginTransaction()
                fm.replace(R.id.content_wrapper, toAdd, "filepicker")
                fm.commit()
            }
        }
    }



}// Required empty public constructor
