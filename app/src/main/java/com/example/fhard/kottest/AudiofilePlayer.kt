package com.example.fhard.kottest


import android.media.MediaMetadataRetriever
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.SeekBar
import android.widget.TextView


/**
 * A simple [Fragment] subclass.
 * Use the [AudiofilePlayer.newInstance] factory method to
 * create an instance of this fragment.
 */
class AudiofilePlayer : Fragment() {

    // TODO: Rename and change types of parameters
    private var mParam1: Uri? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments.getParcelable(ARG_PARAM1)

        }
    }

    fun moveMediaPlayer(m : MediaPlayer, secs : Int, s : SeekBar) {
        var wasPlaying = m.isPlaying
        val curPos = m.currentPosition
        if (wasPlaying) {
            m.pause()
        }
        val newPos = curPos + (secs * 1000)
        if (newPos < 0) {
            m.seekTo(0)
            s.progress = 0
           // wasPlaying = false //tell function not to start player

        } else {
            if (newPos > m.duration){
                m.seekTo(0)
                s.progress = 0
            } else {
                m.seekTo(newPos)
                s.progress = newPos / 1000
            }
        }
        if (wasPlaying){m.start()}
    }





    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view =  inflater!!.inflate(R.layout.fragment_audiofile_player, container, false)
        val seekbar =   view.findViewById(R.id.seekBar) as SeekBar
        val playBtn = view.findViewById(R.id.playButton) as Button
        val trackShower = view.findViewById(R.id.trackNameTv) as TextView
        val mMediaPlayer = MediaPlayer.create(activity.applicationContext, mParam1)
        val metaData = MediaMetadataRetriever()
        metaData.setDataSource(activity.applicationContext, mParam1)
        val trackName = metaData.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE)
        trackShower.text = trackName


        seekbar.max = (mMediaPlayer.duration / 1000)
        seekbar.progress = 0

        val handler = Handler()

        var runner = object : Runnable {
            override fun run() {
           //     if (mMediaPlayer.isPlaying == true) {

                seekbar.progress = mMediaPlayer.currentPosition / 1000
             //   }
                handler.postDelayed(this, 100)
            }
        }

        seekbar.setOnSeekBarChangeListener( object : SeekBar.OnSeekBarChangeListener {
            override fun onStopTrackingTouch(p0: SeekBar?) {

            }

            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onProgressChanged(p0: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) mMediaPlayer.seekTo(progress * 1000)
            }
        })


        playBtn.setOnClickListener {
            if (mMediaPlayer.isPlaying){
                mMediaPlayer.pause()
                playBtn.text = "Play"
            } else {
                mMediaPlayer.start()
                playBtn.text = "Pause"
                activity.runOnUiThread(runner)


            }
        }

        mMediaPlayer.setOnCompletionListener {
            playBtn.text = "Play"
            seekbar.progress = 0
        }

        val longBackBtn = view.findViewById(R.id.backlongButton) as Button
        longBackBtn.setOnClickListener {
            moveMediaPlayer(mMediaPlayer, -5, seekbar)
        }

        val shortBackBtn = view.findViewById(R.id.backsbortButton) as Button
        shortBackBtn.setOnClickListener {
            moveMediaPlayer(mMediaPlayer, -3, seekbar)
        }

        val shortFowardBtn = view.findViewById(R.id.forwardshortButton) as Button
        shortFowardBtn.setOnClickListener {
            moveMediaPlayer(mMediaPlayer, 3, seekbar)
        }

        val longFowradBtn = view.findViewById(R.id.forwardlongButton) as Button
        longFowradBtn.setOnClickListener {
            moveMediaPlayer(mMediaPlayer, 5, seekbar)
        }

        return view
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"


        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @param param1 Parameter 1.
         * *
         * @param param2 Parameter 2.
         * *
         * @return A new instance of fragment AudiofilePlayer.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: Uri?): AudiofilePlayer {
            val fragment = AudiofilePlayer()
            val args = Bundle()
            args.putParcelable(ARG_PARAM1, param1)

            fragment.arguments = args
            return fragment
        }
    }

}// Required empty public constructor
